

<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Cataloging data &mdash; BioLite 1.0.0 documentation</title>
  

  
  
  
  

  

  
  
    

  

  
  
    <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  

  

  
        <link rel="index" title="Index"
              href="genindex.html"/>
        <link rel="search" title="Search" href="search.html"/>
    <link rel="top" title="BioLite 1.0.0 documentation" href="index.html"/>
        <link rel="next" title="Diagnostics" href="diagnostics.html"/>
        <link rel="prev" title="BioLite Reference Manual" href="index.html"/> 

  
  <script src="_static/js/modernizr.min.js"></script>

</head>

<body class="wy-body-for-nav" role="document">

   
  <div class="wy-grid-for-nav">

    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search">
          

          
            <a href="index.html" class="icon icon-home"> BioLite
          

          
          </a>

          
            
            
              <div class="version">
                1.0.0
              </div>
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
              
            
            
              <ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Cataloging data</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#module-biolite.catalog"><code class="docutils literal"><span class="pre">catalog</span></code> Module</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="diagnostics.html">Diagnostics</a></li>
<li class="toctree-l1"><a class="reference internal" href="pipelines.html">Building pipelines</a></li>
<li class="toctree-l1"><a class="reference internal" href="reports.html">Generating reports</a></li>
<li class="toctree-l1"><a class="reference internal" href="wrappers.html">Calling external tools</a></li>
<li class="toctree-l1"><a class="reference internal" href="workflows.html">Automating workflows</a></li>
<li class="toctree-l1"><a class="reference internal" href="internals.html">Internals</a></li>
</ul>

            
          
        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" role="navigation" aria-label="top navigation">
        
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">BioLite</a>
        
      </nav>


      
      <div class="wy-nav-content">
        <div class="rst-content">
          















<div role="navigation" aria-label="breadcrumbs navigation">

  <ul class="wy-breadcrumbs">
    
      <li><a href="index.html">Docs</a> &raquo;</li>
        
      <li>Cataloging data</li>
    
    
      <li class="wy-breadcrumbs-aside">
        
            
            <a href="_sources/catalog.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
    
  </ul>

  
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="cataloging-data">
<h1>Cataloging data<a class="headerlink" href="#cataloging-data" title="Permalink to this headline">¶</a></h1>
<p>The easiest way to interact with the BioLite catalog is using the <cite>catalog</cite>
script packaged with BioLite:</p>
<div class="highlight-default"><div class="highlight"><pre><span></span>$ catalog -h
usage: catalog [-h] {insert,all,search,sizes} ...

Command-line tool for interacting with the agalma catalog.

agalma maintains a &#39;catalog&#39; stored in an SQLite database of metadata
associated with your raw Illumina data, including:

- A unique ID that you make up to reference this data set.
- Paths to the FASTQ files containing the raw forward and reverse reads.
- The species name and NCBI ID.
- The sequencing center where the data was collected.

optional arguments:
  -h, --help            show this help message and exit

commands:
  {insert,all,search,sizes}
    insert              Add a new record to the catalog, or overwrite the
                        existing record with the same id.
    all                 List all catalog entries.
    search              Search all fields (except &#39;paths&#39;) for entries
                        matching the provided pattern, which can include * as
                        a wildcard.
    sizes               List all paths in the catalog, ordered by size on
                        disk.
</pre></div>
</div>
<p>The documentation below describes the <cite>catalog</cite> module, for manually
interacting with the catalog from within a Python script.</p>
<div class="section" id="module-biolite.catalog">
<span id="catalog-module"></span><h2><code class="xref py py-mod docutils literal"><span class="pre">catalog</span></code> Module<a class="headerlink" href="#module-biolite.catalog" title="Permalink to this headline">¶</a></h2>
<p>The BioLite <em>catalog</em> table pairs metadata with the raw NGS data files
(identified by their absolute path on disk). It includes the following:</p>
<ul class="simple">
<li>A <em>unique ID</em> for referencing the data set. If the data is
paired-end Illumina HiSeq data, the ID can be automatically generated using
unique information in the Illumina header.</li>
<li><em>Paths</em> to the raw sequence data. For paired-end Illumina data, this
is expected to be two FASTQ files (possibly compressed) containing the
forward and reverse reads.</li>
<li><em>Notes</em> about the species, the sample preparation and origin, the species,
IDs from NCBI and ITIS taxonomies, and the sequencing
machine and center where the data were collected.</li>
</ul>
<p>The catalog acts as a bridge between the BioLite diagnostics and a more
detailed laboratory information management system (LIMS) for tracking
provenance of sample preparation and data collection upstream of and during
sequencing.  It contains the minimal context needed to associate diagnostics
reports of downstream analyses with the raw sequence data, but without
replicating or reimplementing the full functionality of a LIMS.</p>
<dl class="class">
<dt id="biolite.catalog.CatalogRecord">
<em class="property">class </em><code class="descclassname">biolite.catalog.</code><code class="descname">CatalogRecord</code><span class="sig-paren">(</span><em>id</em>, <em>paths</em>, <em>species</em>, <em>ncbi_id</em>, <em>itis_id</em>, <em>extraction_id</em>, <em>library_id</em>, <em>library_type</em>, <em>individual</em>, <em>treatment</em>, <em>sequencer</em>, <em>seq_center</em>, <em>note</em>, <em>sample_prep</em>, <em>timestamp</em><span class="sig-paren">)</span><a class="headerlink" href="#biolite.catalog.CatalogRecord" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal"><span class="pre">tuple</span></code></p>
<p>A named tuple for holding records from the <a class="reference internal" href="internals.html#catalog-table"><span class="std std-ref">catalog Table</span></a>.</p>
<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.extraction_id">
<code class="descname">extraction_id</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.extraction_id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 5</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.id">
<code class="descname">id</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 0</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.individual">
<code class="descname">individual</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.individual" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 8</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.itis_id">
<code class="descname">itis_id</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.itis_id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 4</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.library_id">
<code class="descname">library_id</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.library_id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 6</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.library_type">
<code class="descname">library_type</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.library_type" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 7</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.ncbi_id">
<code class="descname">ncbi_id</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.ncbi_id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 3</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.note">
<code class="descname">note</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.note" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 12</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.paths">
<code class="descname">paths</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.paths" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 1</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.sample_prep">
<code class="descname">sample_prep</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.sample_prep" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 13</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.seq_center">
<code class="descname">seq_center</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.seq_center" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 11</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.sequencer">
<code class="descname">sequencer</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.sequencer" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 10</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.species">
<code class="descname">species</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.species" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 2</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.timestamp">
<code class="descname">timestamp</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.timestamp" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 14</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.catalog.CatalogRecord.treatment">
<code class="descname">treatment</code><a class="headerlink" href="#biolite.catalog.CatalogRecord.treatment" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 9</p>
</dd></dl>

</dd></dl>

<dl class="function">
<dt id="biolite.catalog.colored">
<code class="descclassname">biolite.catalog.</code><code class="descname">colored</code><span class="sig-paren">(</span><em>string</em>, <em>color</em>, <em>**kwargs</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#colored"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.colored" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.catalog.split_paths">
<code class="descclassname">biolite.catalog.</code><code class="descname">split_paths</code><span class="sig-paren">(</span><em>paths</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#split_paths"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.split_paths" title="Permalink to this definition">¶</a></dt>
<dd><p>Splits a catalog path entry to return a list of paths.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.catalog.insert">
<code class="descclassname">biolite.catalog.</code><code class="descname">insert</code><span class="sig-paren">(</span><em>**kwargs</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#insert"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.insert" title="Permalink to this definition">¶</a></dt>
<dd><p>Insert or update a catalog entry, where keyword arguments specify the
column/value pairs. If an entry for the given ID already exists, then
the specified column/values pairs are used to update the entry. If the
ID does not exist, a new entry is created with the specified values.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.catalog.select">
<code class="descclassname">biolite.catalog.</code><code class="descname">select</code><span class="sig-paren">(</span><em>id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#select"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.select" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns a CatalogRecord object for the given catalog ID, or :keyword:None if
the ID is not found in the catalog.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.catalog.select_all">
<code class="descclassname">biolite.catalog.</code><code class="descname">select_all</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#select_all"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.select_all" title="Permalink to this definition">¶</a></dt>
<dd><p>Yields a list of CatalogRecord objects for all entries in the catalog,
ordered with the default ordering that SQLite provides.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.catalog.search">
<code class="descclassname">biolite.catalog.</code><code class="descname">search</code><span class="sig-paren">(</span><em>string</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#search"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.search" title="Permalink to this definition">¶</a></dt>
<dd><p>Yields a list of CatalogRecord objects for all entries in the catalog
with an indexed column matching the given search <cite>string</cite>. The indexed
columns are all the columns in the catalog except <cite>paths</cite>.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.catalog.make_record">
<code class="descclassname">biolite.catalog.</code><code class="descname">make_record</code><span class="sig-paren">(</span><em>**kwargs</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#make_record"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.make_record" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns a CatalogRecord object by mapping the provided keyword arguments
to field names.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.catalog.print_record">
<code class="descclassname">biolite.catalog.</code><code class="descname">print_record</code><span class="sig-paren">(</span><em>*args</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/catalog.html#print_record"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.catalog.print_record" title="Permalink to this definition">¶</a></dt>
<dd><p>A human-readable printout of CatalogRecord <cite>record</cite>, using colors if the
current tty supports it and the <cite>termcolor</cite> module is installed.</p>
</dd></dl>

</div>
</div>


           </div>
           <div class="articleComments">
            
           </div>
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="diagnostics.html" class="btn btn-neutral float-right" title="Diagnostics" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
        <a href="index.html" class="btn btn-neutral" title="BioLite Reference Manual" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left"></span> Previous</a>
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>
        &copy; Copyright (c) 2012-2017 Brown University. All rights reserved..

    </p>
  </div>
  Built with <a href="http://sphinx-doc.org/">Sphinx</a> using a <a href="https://github.com/snide/sphinx_rtd_theme">theme</a> provided by <a href="https://readthedocs.org">Read the Docs</a>. 

</footer>

        </div>
      </div>

    </section>

  </div>
  


  

    <script type="text/javascript">
        var DOCUMENTATION_OPTIONS = {
            URL_ROOT:'./',
            VERSION:'1.0.0',
            COLLAPSE_INDEX:false,
            FILE_SUFFIX:'.html',
            HAS_SOURCE:  true,
            SOURCELINK_SUFFIX: '.txt'
        };
    </script>
      <script type="text/javascript" src="_static/jquery.js"></script>
      <script type="text/javascript" src="_static/underscore.js"></script>
      <script type="text/javascript" src="_static/doctools.js"></script>

  

  
  
    <script type="text/javascript" src="_static/js/theme.js"></script>
  

  
  
  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.StickyNav.enable();
      });
  </script>
   

</body>
</html>