

<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Diagnostics &mdash; BioLite 1.0.0 documentation</title>
  

  
  
  
  

  

  
  
    

  

  
  
    <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  

  

  
        <link rel="index" title="Index"
              href="genindex.html"/>
        <link rel="search" title="Search" href="search.html"/>
    <link rel="top" title="BioLite 1.0.0 documentation" href="index.html"/>
        <link rel="next" title="Building pipelines" href="pipelines.html"/>
        <link rel="prev" title="Cataloging data" href="catalog.html"/> 

  
  <script src="_static/js/modernizr.min.js"></script>

</head>

<body class="wy-body-for-nav" role="document">

   
  <div class="wy-grid-for-nav">

    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search">
          

          
            <a href="index.html" class="icon icon-home"> BioLite
          

          
          </a>

          
            
            
              <div class="version">
                1.0.0
              </div>
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
              
            
            
              <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="catalog.html">Cataloging data</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Diagnostics</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#initializing">Initializing</a></li>
<li class="toctree-l2"><a class="reference internal" href="#logging-a-record">Logging a record</a></li>
<li class="toctree-l2"><a class="reference internal" href="#provenance">Provenance</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="pipelines.html">Building pipelines</a></li>
<li class="toctree-l1"><a class="reference internal" href="reports.html">Generating reports</a></li>
<li class="toctree-l1"><a class="reference internal" href="wrappers.html">Calling external tools</a></li>
<li class="toctree-l1"><a class="reference internal" href="workflows.html">Automating workflows</a></li>
<li class="toctree-l1"><a class="reference internal" href="internals.html">Internals</a></li>
</ul>

            
          
        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" role="navigation" aria-label="top navigation">
        
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">BioLite</a>
        
      </nav>


      
      <div class="wy-nav-content">
        <div class="rst-content">
          















<div role="navigation" aria-label="breadcrumbs navigation">

  <ul class="wy-breadcrumbs">
    
      <li><a href="index.html">Docs</a> &raquo;</li>
        
      <li>Diagnostics</li>
    
    
      <li class="wy-breadcrumbs-aside">
        
            
            <a href="_sources/diagnostics.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
    
  </ul>

  
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="module-biolite.diagnostics">
<span id="diagnostics"></span><h1>Diagnostics<a class="headerlink" href="#module-biolite.diagnostics" title="Permalink to this headline">¶</a></h1>
<p>Diagnostics usually come in the form of plots or summary statistics.
They can serve many purposes, such as:</p>
<ul class="simple">
<li>diagnosing problems in sample preparation and optimizing future preparations;</li>
<li>providing feedback on the sequencing itself, e.g. on read quality;</li>
<li>implementing &#8216;sanity checks&#8217; at intermediate steps of analysis;</li>
<li>finding optimal parameters by comparing previous runs;</li>
<li>recording computational and storage demands, and predicting future demands.</li>
</ul>
<p>The <em>diagnostics</em> database table archives summary statistics that can be
accessed across multiple stages of a pipeline, from different pipelines, and in
HTML reports.</p>
<p>A diagnostics record looks like:</p>
<div class="highlight-default"><div class="highlight"><pre><span></span><span class="n">catalog_id</span> <span class="o">|</span> <span class="n">run_id</span> <span class="o">|</span> <span class="n">entity</span> <span class="o">|</span> <span class="n">attribute</span> <span class="o">|</span> <span class="n">value</span> <span class="o">|</span> <span class="n">timestamp</span>
</pre></div>
</div>
<p>The <cite>entity</cite> field acts as a namespace to prevent attribute collisions, since
the same attribute name can arise multiple times within a pipeline run.</p>
<p>When running a BioLite pipeline, the default entity is the pipeline name plus
the stage name, so that values can be traced to the pipeline and stage during
which they were entered. Entries in the diagnostics table can include paths to
derivative files, which can be summaries of intermediate files that are used to
generate reports or intermediate data files that serve as input to other stages
and pipelines.</p>
<div class="section" id="initializing">
<h2>Initializing<a class="headerlink" href="#initializing" title="Permalink to this headline">¶</a></h2>
<p>Before logging to diagnostics, your script must initialize this module with a
BioLite catalog ID and a name for the run using the <cite>init</cite> method. This will
return a new run ID from the <a class="reference internal" href="internals.html#runs-table"><span class="std std-ref">runs Table</span></a>. Optionally, you can pass an
existing run ID to <cite>init</cite> to continue a previous run.</p>
<p>Diagnostics are automatically initialized by the Pipeline and IlluminaPipeline
classes in the <a class="reference internal" href="pipelines.html#pipeline-module"><span class="std std-ref">pipeline Module</span></a>.</p>
</div>
<div class="section" id="logging-a-record">
<h2>Logging a record<a class="headerlink" href="#logging-a-record" title="Permalink to this headline">¶</a></h2>
<p>Use the <cite>log</cite> function described below.</p>
<p>Detailed system utilization statistics, including memory high-water marks and
compute wall-time are recorded automatically (by the wrapper base class) for
any wrapper that your pipeline calls, and for the overall pipeline itself.</p>
</div>
<div class="section" id="provenance">
<h2>Provenance<a class="headerlink" href="#provenance" title="Permalink to this headline">¶</a></h2>
<p>Because every wrapper call is automatically logged, the diagnostics table holds
a complete non-executable history of the analysis, which complements the
original scripts that were used to run the analysis. In combination, the
diagnostics table and original scripts provide provenance for all analyses.</p>
<dl class="class">
<dt id="biolite.diagnostics.OutputPattern">
<em class="property">class </em><code class="descclassname">biolite.diagnostics.</code><code class="descname">OutputPattern</code><span class="sig-paren">(</span><em>re</em>, <em>entity</em>, <em>attr</em><span class="sig-paren">)</span><a class="headerlink" href="#biolite.diagnostics.OutputPattern" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal"><span class="pre">tuple</span></code></p>
<dl class="attribute">
<dt id="biolite.diagnostics.OutputPattern.attr">
<code class="descname">attr</code><a class="headerlink" href="#biolite.diagnostics.OutputPattern.attr" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 2</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.OutputPattern.entity">
<code class="descname">entity</code><a class="headerlink" href="#biolite.diagnostics.OutputPattern.entity" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 1</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.OutputPattern.re">
<code class="descname">re</code><a class="headerlink" href="#biolite.diagnostics.OutputPattern.re" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 0</p>
</dd></dl>

</dd></dl>

<dl class="class">
<dt id="biolite.diagnostics.Run">
<em class="property">class </em><code class="descclassname">biolite.diagnostics.</code><code class="descname">Run</code><span class="sig-paren">(</span><em>done</em>, <em>id</em>, <em>catalog_id</em>, <em>name</em>, <em>hostname</em>, <em>username</em>, <em>timestamp</em>, <em>hidden</em><span class="sig-paren">)</span><a class="headerlink" href="#biolite.diagnostics.Run" title="Permalink to this definition">¶</a></dt>
<dd><p>Bases: <code class="xref py py-class docutils literal"><span class="pre">tuple</span></code></p>
<dl class="attribute">
<dt id="biolite.diagnostics.Run.catalog_id">
<code class="descname">catalog_id</code><a class="headerlink" href="#biolite.diagnostics.Run.catalog_id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 2</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.done">
<code class="descname">done</code><a class="headerlink" href="#biolite.diagnostics.Run.done" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 0</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.hidden">
<code class="descname">hidden</code><a class="headerlink" href="#biolite.diagnostics.Run.hidden" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 7</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.hostname">
<code class="descname">hostname</code><a class="headerlink" href="#biolite.diagnostics.Run.hostname" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 4</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.id">
<code class="descname">id</code><a class="headerlink" href="#biolite.diagnostics.Run.id" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 1</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.name">
<code class="descname">name</code><a class="headerlink" href="#biolite.diagnostics.Run.name" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 3</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.timestamp">
<code class="descname">timestamp</code><a class="headerlink" href="#biolite.diagnostics.Run.timestamp" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 6</p>
</dd></dl>

<dl class="attribute">
<dt id="biolite.diagnostics.Run.username">
<code class="descname">username</code><a class="headerlink" href="#biolite.diagnostics.Run.username" title="Permalink to this definition">¶</a></dt>
<dd><p>Alias for field number 5</p>
</dd></dl>

</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.str2list">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">str2list</code><span class="sig-paren">(</span><em>data</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#str2list"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.str2list" title="Permalink to this definition">¶</a></dt>
<dd><p>Converts a diagnostics string with key <cite>name</cite> in <cite>self.data</cite> into a
list, by parsing it as a typical Python list representation
<code class="samp docutils literal"><span class="pre">[item1,</span> <span class="pre">item2,</span> <span class="pre">...</span> <span class="pre">]</span></code>.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.get_run_id">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">get_run_id</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#get_run_id"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.get_run_id" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns the <cite>run_id</cite> (as a string)</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.get_entity">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">get_entity</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#get_entity"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.get_entity" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns the current <cite>entity</cite> as a dot-delimited string.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.init">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">init</code><span class="sig-paren">(</span><em>catalog_id</em>, <em>name</em>, <em>run_id=None</em>, <em>workdir='/Users/mhowison/Drive/code/biolite/doc'</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#init"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.init" title="Permalink to this definition">¶</a></dt>
<dd><p>By default, appends to a file <cite>diagnostics.txt</cite> in the current working
directory, but you can override this with the <cite>workdir</cite> argument.</p>
<p>You must specify a <cite>catalog_id</cite> and a <cite>name</cite> for the run. If no <cite>run_id</cite>
is specified, an auto-incremented run ID will be allocated by inserting
a new row into the <a class="reference internal" href="internals.html#runs-table"><span class="std std-ref">runs Table</span></a>.</p>
<p>Returns the <cite>run_id</cite> (as a string).</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.check_init">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">check_init</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#check_init"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.check_init" title="Permalink to this definition">¶</a></dt>
<dd><p>Aborts if the biolite.diagnostics.init() has not been called yet.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.merge">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">merge</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#merge"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.merge" title="Permalink to this definition">¶</a></dt>
<dd><p>Merges the diagnostics and program caches into the SQLite database.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.merge_cwd">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">merge_cwd</code><span class="sig-paren">(</span><em>run_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#merge_cwd"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.merge_cwd" title="Permalink to this definition">¶</a></dt>
<dd><p>Merges the &#8216;diagnostics.txt&#8217; and &#8216;programs.txt&#8217; in the current working
directory (cwd) into the diagnostics database.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.load_cache">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">load_cache</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#load_cache"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.load_cache" title="Permalink to this definition">¶</a></dt>
<dd><p>Similar to a merge, but loads the local diagnostics file into an
in-memory cache instead of the SQLite database.</p>
<p>Uses the filename specified with <cite>name</cite>, or the file <cite>diagnostics.txt</cite> in
the current working directory (default).</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log</code><span class="sig-paren">(</span><em>attribute</em>, <em>value</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log" title="Permalink to this definition">¶</a></dt>
<dd><p>Log an <cite>attribute</cite>/<cite>value</cite> pair in the diagnostics using the currently set
<cite>entity</cite>. The pair is written to the local diagnostics text file and also
into the local in-memory cache.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_entity">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_entity</code><span class="sig-paren">(</span><em>attribute</em>, <em>value</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_entity"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_entity" title="Permalink to this definition">¶</a></dt>
<dd><p>Log an <cite>attribute</cite>/<cite>value</cite> pair in the diagnostics, where the <cite>attribute</cite>
can contain an <cite>entity</cite> that is separated from the attribute name by dots.
Example:</p>
<blockquote>
<div>log_entity(&#8216;a.b.x&#8217;, 1)</div></blockquote>
<p>would store the attribute/value pair (x,1) in an entity &#8216;a.b&#8217; appended to
the current <cite>entity</cite>.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_exit">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_exit</code><span class="sig-paren">(</span><em>attribute</em>, <em>value</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_exit"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_exit" title="Permalink to this definition">¶</a></dt>
<dd><p>Log an <cite>attribute</cite>/<cite>value</cite> pair in the diagnostics in the special EXIT
entity that is used to pass values between pipelines.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_path">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_path</code><span class="sig-paren">(</span><em>path</em>, <em>log_prefix=None</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_path"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_path" title="Permalink to this definition">¶</a></dt>
<dd><p>Logs a <cite>path</cite> by writing these attributes at the current <cite>entity</cite>, with
an optional prefix for this entry:
1) the full <cite>path</cite> string
2) the full <cite>path</cite> string, converted to an absolute path by os.path.abspath()
3) the <cite>size</cite> of the file/directory at the path (according to <cite>os.stat</cite>)
4) the <cite>access time</cite> of the file/directory at the path (according to <cite>os.stat</cite>)
5) the <cite>modify time</cite> of the file/directory at the path (according to <cite>os.stat</cite>)
6) the <cite>permissions</cite> of the file/directory at the path (according to <cite>os.stat</cite>)</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_dict">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_dict</code><span class="sig-paren">(</span><em>d</em>, <em>prefix=None</em>, <em>filter=False</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_dict"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_dict" title="Permalink to this definition">¶</a></dt>
<dd><p>Log a dictionary <cite>d</cite> by calling <cite>log</cite> for each key/value pair.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_rusage">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_rusage</code><span class="sig-paren">(</span><em>log_prefix='__rusage__'</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_rusage"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_rusage" title="Permalink to this definition">¶</a></dt>
<dd><p>Log the current resource usage with a timestamp. Returns a <cite>dict</cite> with
the fields:</p>
<ul class="simple">
<li>timestamp</li>
<li>systime (ru_stime)</li>
<li>usertime (ru_utime)</li>
<li>maxrss (ru_maxrss)</li>
</ul>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_program_version">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_program_version</code><span class="sig-paren">(</span><em>name</em>, <em>version</em>, <em>path</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_program_version"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_program_version" title="Permalink to this definition">¶</a></dt>
<dd><p>Enter the version string and a hash of the binary file at <cite>path</cite> into the
programs table.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.log_program_output">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">log_program_output</code><span class="sig-paren">(</span><em>filename</em>, <em>patterns=None</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#log_program_output"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.log_program_output" title="Permalink to this definition">¶</a></dt>
<dd><p>Read backwards through a program&#8217;s output to find any [biolite] markers,
then log their key=value pairs in the diagnostics.</p>
<p>A marker can specify an entity suffix with the form [biolite.suffix].</p>
<p>[biolite.profile] markers are handled specially, since mem= and vmem=
entries need to be accumulated. These are inserted into a program&#8217;s output
on Linux systems by the preloaded memusage.so library.</p>
<p>You can optionally include a list of additional patterns, specified as
OutputPattern tuples with:</p>
<blockquote>
<div>(regular expression string, entity, attribute)</div></blockquote>
<p>and the first line of program output matching the pattern will be logged
to that entity and attribute name. The value will be the subexpressions
matched by the regular expression, either a single value if there is one
subexpression, or a string of the tuple if there are more.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup</code><span class="sig-paren">(</span><em>run_id</em>, <em>entity</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns a dictionary of <cite>attribute</cite>/<cite>value</cite> pairs for the given <cite>run_id</cite> and
<cite>entity</cite> in the SQLite database.</p>
<p>Returns an empty dictionary if no records are found.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.local_lookup">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">local_lookup</code><span class="sig-paren">(</span><em>entity</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#local_lookup"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.local_lookup" title="Permalink to this definition">¶</a></dt>
<dd><p>Similar to <cite>lookup</cite>, but queries the in-memory cache instead of the SQLite
database. This can provide lookups when the local diagnostics text file
has not yet been merged into the SQLite database (for instance, after
restarting a pipeline that never completed, and hence never reached a
diagnostics merge).</p>
<p>Returns an empty dictionary if no records are found.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_like">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_like</code><span class="sig-paren">(</span><em>run_id</em>, <em>entity</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_like"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_like" title="Permalink to this definition">¶</a></dt>
<dd><p>Similar to <cite>lookup</cite>, but allows for wildcards in the entity name (either
the SQL &#8216;%&#8217; wildcard or the more standard UNIX &#8216;*&#8217; wildcard).</p>
<p>Returns a dictinoary of dictionaries keyed on [<cite>entity</cite>][<cite>attribute</cite>].</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_by_id">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_by_id</code><span class="sig-paren">(</span><em>catalog_id</em>, <em>entity</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_by_id"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_by_id" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_attribute">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_attribute</code><span class="sig-paren">(</span><em>run_id</em>, <em>attribute</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_attribute"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_attribute" title="Permalink to this definition">¶</a></dt>
<dd><p>Returns each value for the given <cite>attribute</cite> found in all entities for
the given <cite>run_id</cite>, as an iterator of (entity, value) tuples.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_entities">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_entities</code><span class="sig-paren">(</span><em>run_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_entities"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_entities" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_pipelines">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_pipelines</code><span class="sig-paren">(</span><em>run_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_pipelines"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_pipelines" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_run">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_run</code><span class="sig-paren">(</span><em>run_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_run"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_run" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_runs">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_runs</code><span class="sig-paren">(</span><em>catalog_id=None</em>, <em>name=None</em>, <em>order='ASC'</em>, <em>hidden=True</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_runs"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_runs" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_prev_run">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_prev_run</code><span class="sig-paren">(</span><em>catalog_id</em>, <em>previous</em>, <em>*args</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_prev_run"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_prev_run" title="Permalink to this definition">¶</a></dt>
<dd><blockquote>
<div>Lookup either the specific <cite>previous</cite> run, or if <cite>previous</cite> is None,</div></blockquote>
<p>the latest run of a pipeline with a name in the <cite>args</cite> list.</p>
<blockquote>
<div><p>Will only return a run that is finished and not hidden.</p>
<p>Returns None if no run can be found.</p>
</div></blockquote>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_prev_val">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_prev_val</code><span class="sig-paren">(</span><em>catalog_id</em>, <em>previous</em>, <em>value</em>, <em>key</em>, <em>*args</em>, <em>**kwargs</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_prev_val"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_prev_val" title="Permalink to this definition">¶</a></dt>
<dd><p>Determine a value based on the following order:</p>
<ul class="simple">
<li>use the specified <cite>value</cite> if it is not None</li>
<li>lookup the <cite>previous</cite> run ID if it is not None, and select <cite>key</cite> in the
exit diagnostics</li>
<li>lookup the latest run of a pipeline in <cite>*args</cite> and select <cite>key</cite> from
the exit diagnostics</li>
</ul>
<p>Will only search runs that are finished and not hidden.</p>
<p>Returns None if no value can be determined.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.lookup_insert_size">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">lookup_insert_size</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#lookup_insert_size"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.lookup_insert_size" title="Permalink to this definition">¶</a></dt>
<dd><p>For tools that need insert sizes, use available estimates from the
diagnostics database, or resort to the default values in the BioLite
configuration file.</p>
<p>Returns a Struct with the fields <cite>mean</cite>, <cite>stddev</cite> and <cite>max</cite>.</p>
</dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.dump">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">dump</code><span class="sig-paren">(</span><em>run_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#dump"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.dump" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.dump_commands">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">dump_commands</code><span class="sig-paren">(</span><em>run_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#dump_commands"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.dump_commands" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.dump_by_id">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">dump_by_id</code><span class="sig-paren">(</span><em>catalog_id</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#dump_by_id"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.dump_by_id" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.dump_all">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">dump_all</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#dump_all"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.dump_all" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.hide_run">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">hide_run</code><span class="sig-paren">(</span><em>*args</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#hide_run"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.hide_run" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.unhide_run">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">unhide_run</code><span class="sig-paren">(</span><em>*args</em><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#unhide_run"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.unhide_run" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

<dl class="function">
<dt id="biolite.diagnostics.dump_programs">
<code class="descclassname">biolite.diagnostics.</code><code class="descname">dump_programs</code><span class="sig-paren">(</span><span class="sig-paren">)</span><a class="reference internal" href="_modules/biolite/diagnostics.html#dump_programs"><span class="viewcode-link">[source]</span></a><a class="headerlink" href="#biolite.diagnostics.dump_programs" title="Permalink to this definition">¶</a></dt>
<dd></dd></dl>

</div>
</div>


           </div>
           <div class="articleComments">
            
           </div>
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="pipelines.html" class="btn btn-neutral float-right" title="Building pipelines" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
        <a href="catalog.html" class="btn btn-neutral" title="Cataloging data" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left"></span> Previous</a>
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>
        &copy; Copyright (c) 2012-2017 Brown University. All rights reserved..

    </p>
  </div>
  Built with <a href="http://sphinx-doc.org/">Sphinx</a> using a <a href="https://github.com/snide/sphinx_rtd_theme">theme</a> provided by <a href="https://readthedocs.org">Read the Docs</a>. 

</footer>

        </div>
      </div>

    </section>

  </div>
  


  

    <script type="text/javascript">
        var DOCUMENTATION_OPTIONS = {
            URL_ROOT:'./',
            VERSION:'1.0.0',
            COLLAPSE_INDEX:false,
            FILE_SUFFIX:'.html',
            HAS_SOURCE:  true,
            SOURCELINK_SUFFIX: '.txt'
        };
    </script>
      <script type="text/javascript" src="_static/jquery.js"></script>
      <script type="text/javascript" src="_static/underscore.js"></script>
      <script type="text/javascript" src="_static/doctools.js"></script>

  

  
  
    <script type="text/javascript" src="_static/js/theme.js"></script>
  

  
  
  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.StickyNav.enable();
      });
  </script>
   

</body>
</html>